import * as THREE from 'three';
import {jsPDF} from 'jspdf'
import {OBJLoader2} from 'three/examples/jsm/loaders/OBJLoader2';


function generatePDF(image, maxNrOfPages, setProgress) {
    const img = document.createElement("img");
    img.onerror = () => console.log("failed loading image!")
    img.onload = () => {
        const aspectRatio = calculateAspectRatio(img)
        generate(maxNrOfPages, image, aspectRatio, setProgress)
    }
    img.src = image
}

function generate(maxNrOfPages, image, aspectRatio, setProgress) {
    let maxNrOfTriangles = 16 * maxNrOfPages

    const renderer = setupRenderer()
    const camera = setupCamera()
    const light = setupLight()
    const scene = setupScene(light)
    const render = () => renderer.render( scene, camera );

    const {triangles, rows, cols} = calculateRowsAndColumns(aspectRatio, maxNrOfTriangles)
    console.log(triangles, 'triangles, ', rows, ' rows and', cols, ' columns');
    console.log('resulting size:', cols, 'x', rows*0.6, 'cm')
    let pages = Math.ceil(triangles / 16)

    const modelName = 'models/paper_model'
    loadCalibratie(image, modelName, scene, triangles, rows, cols)

    saveToPdf(pages, camera, render, triangles, rows, cols, setProgress);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function calculateAspectRatio(image) {
    const aspectRatio = image.width / image.height
    console.log("apsect ratio:", aspectRatio)
    return aspectRatio
}

function calculateRowsAndColumns(aspectRatio, maxNrOfTriangles) {
    const triangleRatio = 1/0.6

    let rows = 1
    let cols = 2
    let usedTriangles = 2
    while(true) {
        let currentRatio = cols / rows * triangleRatio
        let newrows = rows
        let newcols = cols
        if(currentRatio < aspectRatio ) {
            newcols += 1
        } else {
            newrows += 1
        }
        let neededTriangles = calculateTriangles(newrows, newcols)
        if (neededTriangles <= maxNrOfTriangles) {
            usedTriangles = neededTriangles
            rows = newrows
            cols = newcols 
        } else {
            break
        }
    }
    return {triangles: usedTriangles, rows: rows, cols: cols}
}

function calculateTriangles(rows, cols) {
    if (rows % 2 === 0) {
        return (cols - 0.5) * rows
    } else {
        return (cols - 0.5) * (rows + 1) - (cols -1)
    }
}

function setupCamera() {
    const left = 0;
    const right = 1;
    const top = 1
    const bottom = 0
    const near = 0.1;
    const far = 10;
    const camera = new THREE.OrthographicCamera(left, right, top, bottom, near, far)
    camera.position.z = 2;

    return camera
}

function setupLight() {
    const intensity = 1.5
    const light = new THREE.AmbientLight('white', intensity)

    return light
}

function setupScene(light) {
    const scene = new THREE.Scene();
    scene.background = new THREE.Color('white')
    scene.add(light);

    return scene
}

function setupRenderer() {
    const canvas = document.querySelector('#canvas');

    return new THREE.WebGLRenderer({
        canvas,     
        preserveDrawingBuffer: true 
    });
}

async function saveToPdf(pages, camera, render, triangles, rows, cols, setProgress) {
    let doc = new jsPDF();
    const canvas = document.querySelector('#canvas');
    doc.setFontSize(8)
    
    await sleep(100)
    render()
    doc.addImage(canvas, "JPEG", 0, 0, 210, 297)
    addLabels(doc, 0, triangles, rows, cols);
    setProgress(Math.floor(1/pages * 100))

    for (let page = 1; page < pages; page++) {
        setTimeout(() => {
            camera.bottom += 1
            camera.top += 1
            camera.updateProjectionMatrix()
            render()
            console.log('adding page', page)
            doc.insertPage(1)
            doc.addImage(canvas, "JPEG", 0, 0, 210, 297)
            addLabels(doc, page, triangles, rows, cols);
            setProgress(Math.floor((page + 1)/pages * 100))
            if (page === pages - 1) {
                doc.save('origami.pdf')
            }
        })
    }

}

function addLabels(doc, page, triangles, rows, cols) {
    let labelsDone = page * 16
    
    for ( let r = 3; r >= 0; r--) {
        for ( let c = 0; c < 4; c++) {
            let x1 = (c + 11/16) /4 * 210
            let y1 = (r + 0.65) /4 * 297
            let x2 = (c + 5/16 ) /4 * 210
            let y2 = (r + 5/8 + 1/16) /4 * 297

            let {col, row} = getRowAndColumn(labelsDone, cols)
            let text = ''
            if(col >= 26) {
                text += String.fromCharCode(65 + Math.floor(col/26) - 1)
                col %= 26
            }
            text += String.fromCharCode(65 + col) + (row + 1)
            doc.text(text, x1, y1, {align: 'center', angle: -45})
            doc.text(text, x2, y2, {align: 'center', angle: 45 })
            labelsDone += 1
            if (labelsDone === triangles) return
        }
    }
}

function loadCalibratie(img, modelName, scene, triangles, triRows, triCols) {
    const objLoader = new OBJLoader2();
    const textureLoader = new THREE.TextureLoader()
    const texture = textureLoader.load(img)

    const textureMaterial = new THREE.MeshBasicMaterial({
        map: texture,
    });
    let materials = {}
    materials.uvUnWrapped = textureMaterial; 
    materials.Black = new THREE.MeshBasicMaterial({color:'black'})
    materials.White = new THREE.MeshBasicMaterial({color:'white'})

    objLoader.addMaterials(materials);
    const objFile = require('./' + modelName + '.obj')
    objLoader.load(objFile, (root) => {
        const pijl = JSON.parse(JSON.stringify(root.children[0].geometry.attributes))

        let materials = JSON.parse(JSON.stringify(root.children[0].geometry.groups))
        let nrMaterials = materials.length

        for (let p = 1; p < triangles; p++) {
            for (let m = 0; m < nrMaterials; m++) {
                let index = p*nrMaterials + m
                let lastStart = materials[index - 1].start
                let lastCount = materials[index -1].count
                let count = materials[m].count
                let matIndex = materials[m].materialIndex
                materials[index] = {'start': lastStart + lastCount, 'count': count, 'materialIndex': matIndex}
            }
        }
        root.children[0].geometry.groups = materials

        let newposition = new Float32Array(pijl.position.array.length*triangles)
        let newnormal = new Float32Array(pijl.normal.array.length*triangles)
        let newuv = new Float32Array(pijl.uv.array.length*triangles)


        let attributes =  root.children[0].geometry.attributes

        let position = root.children[0].geometry.attributes.position.array
        let normal = root.children[0].geometry.attributes.normal.array
        let uv = root.children[0].geometry.attributes.uv.array
        let vertices = position.length
        let columns = 4
        let rows = triangles/columns

        for(let y = 0; y < rows; y++) {
            for(let x = 0; x < columns; x++) {
                for(let i = 0; i < vertices; i++) {
                    let index = vertices * (columns* y+ x) + i
                    newnormal[index] = normal[i]
                    if(i % 3 === 0) {
                        newposition[index] = (position[i] / 0.707 + x) / columns  
                    }
                    if(i % 3 === 1) {
                        newposition[index] = (position[i] + y + 1) / 4
                    }
                }
            }
        }

        let horSquares = 4
        let vertSquares = 4

        for(let y = 0; y < rows; y++) {
            for(let x = 0; x < columns; x++) {
                for(let i = 0; i < uv.length; i++) {

                    let index = uv.length * (columns* y + x) + i
                    if(i % 2 === 0) {
                        newuv[index] = (uv[i] + x) / horSquares
                    }
                    if(i % 2 === 1) {
                        newuv[index] = (uv[i] + y) / vertSquares
                    }
                   
                }
            }
        }
        
        for(let t = 0; t < triangles; t++) {
            for(let i = 0; i < uv.length; i++) {
                const {row, col} = getRowAndColumn(t, triCols)            

                let index = uv.length * t + i
                if(i % 2 === 0) {
                    newuv[index] = (uv[i] + col) / triCols
                    if(row % 2 === 1) {
                        newuv[index] += 0.5 / triCols
                    }
                }
                if(i % 2 === 1) {
                    newuv[index] = (uv[i] + row) / triRows
                }

                
            }
        }

        attributes.position.count *= triangles
        attributes.normal.count *= triangles
        attributes.uv.count *= triangles

        root.children[0].geometry.attributes.position.array = newposition
        root.children[0].geometry.attributes.normal.array = newnormal
        root.children[0].geometry.attributes.uv.array = newuv
        scene.add(root)
    })
}

function getRowAndColumn(triangleIndex, cols) {
    let trianglesInTworows = 2 * cols - 1
    let nrDoubleRows = Math.floor(triangleIndex / trianglesInTworows)
    let row = 2 * nrDoubleRows 
    let col
    let newIndex = triangleIndex - nrDoubleRows * trianglesInTworows

    if (newIndex < cols) {
        col = newIndex
    } else {
        row += 1
        col = newIndex - cols
    }
    return {col: col, row: row}
}

export default generatePDF
