import React from 'react';
import './App.css';
import generatePDF from './OrigamiPdf'
import ImageLoader from './components/ImageLoader'
import ProgressBar from './components/ProgressBar'


function App() {
  let [image, setImage] = React.useState();
  let [pages, setPages] = React.useState(10);
  let [progess, setProgess] = React.useState(0)

  return (
        <div>
          <label>Number of A4 pages</label> <br></br>
          <input type="number" value={pages} onChange={(e) => setPages(e.target.value)} />
          <ImageLoader onLoad={(img) => setImage(img)}/>
          <button id="PDFbutton" onClick={() => generatePDF(image, pages, setProgess)}> 
            Generate PDF
          </button> 
          <ProgressBar progress={progess}/>
          <canvas hidden id="canvas" width="2100" height="2970"></canvas>
        </div>
  );
}

export default App;
