import React from "react"

const ProgressBar = props => {
    const progress = props.progress
    return(
        <>
        <br></br>
        <progress value={progress} max="100"></progress> 
        <br></br>
        </>
    )
}

export default ProgressBar