import React from 'react'

const ImageLoader = props => {
    const handleUpload = (event) => {
        let file = event.target.files[0]

        let reader = new FileReader()
        reader.addEventListener('load', () => {
            showImage(reader.result)
            props.onLoad(reader.result)
        })
        reader.readAsDataURL(file)
    }

    const showImage = (file) => {
        let im = document.getElementById('img')
        im.src=file
    }

    return (
        <div> 
            <label>Input image</label> <br></br>
            <input type="file" id="image" accept="image/*" onChange={(event) => handleUpload(event)}></input>
            <br/>
            <img id='img' width="200px" alt=""></img>
        </div>
    )
}

export default ImageLoader
